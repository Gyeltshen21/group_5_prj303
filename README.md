# PRJ303

# Description

This project is written as a teaching case study for use in a PRJ303, either in a six semester course, or possibly a course in GCIT. It is aimed at the student of Gyalposhing College Of Information Technology to utilize data analytics technologies and life cycle to extract feature, pre-process, clean, analyse, interpret and visualize a large real-world raw datasets to derive a conclusion regarding the information they hold.

# GROUP NUMBER 5 

### Project Topic: Handwritten Dzongkha Alphabet Recognition System using Deep Learning

## Team Profile
Project Guide: Mrs. Sonam Wangmo
<br>
<img src="Screenshoot/sonamwangmo.jpg" width="250" height="250" /> 
<br>
Project Leader Mr. Tshering Penjor
<br>
<img src="Screenshoot/Penjor.jpeg" width="250" height="250" /> 
<br>
Frontend Developer Mr. Tenzin Kelzang
<br>
<img src="Screenshoot/tenzinkelzang.jpeg" width="250" height="250" /> 
<br>
Lead Programmer Mr. Dorji Gyeltshen
<br>
<img src="Screenshoot/DG.jpeg" width="250" height="250" /> 
<br>
UI/UX Design Mrs. Sonam Choki
<br>
<img src="Screenshoot/sonamchoki.jpeg" width="250" height="250" /> 	

# Links
Click here to view APP folder:(https://gitlab.com/Gyeltshen21/group_5_prj303/-/tree/main/Application/handwritten)

Click here to view WEBSITE folder:(https://gitlab.com/Gyeltshen21/group_5_prj303/-/tree/main/Website/Handwritten_Website)

Click here to download dataset:(https://gitlab.com/Gyeltshen21/group_5_prj303/-/tree/main/DataSet)