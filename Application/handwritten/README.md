# Handwritten Dzongkha Alphabet Recognition System using Deep Learning

*Gyalpozhing College of Information 
Technology(Bachelor of Science in Information 
Technology(BScIT)) Year 3 Project*

# Description

There are many research and applications of handwritten OCR in various languages such as the Devanagari text of India and the Farsi text of Arab, there has been no research conducted towards the Dzongkha alphabet of Bhutan. To address this issue and to promote the Dzongkha language, computerization of the language is a must and OCR is one such solution to overcome this obstacle. Handwritten Dzongkha Alphabets Recognition System (གསལ་བྱེད) is an application built for our Third Year Project, where by the application allow us to choose a handwritten image and make prediction. Not only that it allow users to draw and Dzongkha letter and make prediction.

# Technology Used
1. Google colab/Jupyter notebook
2. Tensorflow
3. Visual Studio Code
4. Flutter
5. Python
6. Django 
7. OpenCV
8. Dart

## Installation
Download : 
(https://gitlab.com/Gyeltshen21/group_5_prj303/-/blob/main/Screenshoot/Handwritten.apk)

## Video
Watch video here : 
(https://youtu.be/1aiiCUmvyjQ)

## Screenshoot of Mobile Application
<img src="Screenshoot/Home.jpeg" width="250" height="400" /> 
<img src="Screenshoot/Menu.jpeg" width="250" height="400" />
<img src="Screenshoot/Drawing1.jpeg" width="250" height="400" />
<img src="Screenshoot/Drawing.jpeg" width="250" height="400" />
<img src="Screenshoot/Predicted.jpeg" width="250" height="400" />
<img src="Screenshoot/Gallery1.jpeg" width="250" height="400" />
<img src="Screenshoot/Gallery.jpeg" width="250" height="400" />
<img src="Screenshoot/Predicted1.jpeg" width="250" height="400" />
<img src="Screenshoot/Team_Profile.jpeg" width="250" height="400" />
<img src="Screenshoot/Help.jpeg" width="250" height="400" />

# Poster
<img src="Screenshoot/Poster.jpeg" width="250" height="400" />
