import 'package:flutter/material.dart';
import 'package:handwritten/navbar/navbar.dart';

class AboutUs extends StatelessWidget {
  const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: const NavBar(),
        appBar: AppBar(
          titleSpacing: 1,
          centerTitle: false,
          title: const Text(
            "Dzongkha Handwritten Character",
            style: TextStyle(
              fontSize: 18.0,
              fontFamily: 'Roboto-Thin',
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          backgroundColor: const Color.fromARGB(255, 61, 85, 222),
          elevation: 0,
        ),
        body: Card(
            margin: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
            child: Column(
              children: const <Widget>[
                Text(
                  "The logo of the app will be displayed once we click on our app button which will be followed by two options on the homepage namely :",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'Roboto-Thin',
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "1. Please choose an image from your gallery.\n 2. Click here to draw a letter.",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'Roboto-Thin',
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'While selecting the first option that is “Please choose an image from your gallery”, we can select any image from our gallery of a Dzongkha hand written character and click on the “predict” button below. Based on the character we select, the letter will be predicted along with the accuracy. If we select the picture other than the Dzongkha Handwritten character, the message “Unknown” will be displayed since there is no such letter in Dzongkha Character.',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'Roboto-Thin',
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Similarly, while selecting the second option that is “Click here to draw a letter”, a blank space will be displayed along with the two options namely “Predict” and “clear” where we need to draw the Dzongkha character in the space. We can clear the letter we have drawn on the space provided above to draw a new one, else we can directly predict the character with its accuracy by clicking on the predict button. We can go back to Home page again if we want to recheck or re predict the letters all over again.",
                  maxLines: 7,
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'Roboto-Thin',
                    color: Colors.black,
                  ),
                ),
              ],
            )));
  }
}
