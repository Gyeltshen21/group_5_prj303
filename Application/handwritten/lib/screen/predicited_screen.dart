import 'package:flutter/material.dart';
import 'package:handwritten/navbar/navbar.dart';

class PredictedScreen extends StatefulWidget {
  final String name;
  final String confidence;
  const PredictedScreen(
      {Key? key, required this.name, required this.confidence})
      : super(key: key);

  @override
  State<PredictedScreen> createState() =>
      _PredictedScreenState(name: name, confidence: confidence);
}

class _PredictedScreenState extends State<PredictedScreen> {
  final String name;
  final String confidence;
  String? message = "";
  String accuracy = "";
  _PredictedScreenState({required this.name, required this.confidence});
  @override
  void initState() {
    super.initState();
    check();
  }

  void check() {
    Map<String, String> dict = {
      "Unknown": "No such",
      "ཀ": "First",
      "ཁ": "Second",
      "ག": "Third",
      "ང": "Fourth",
      "ཅ": "Fifth",
      "ཆ": "Sixth",
      "ཇ": "Seventh",
      "ཉ": "Eighth",
      "ཏ": "Nineth",
      "ཐ": "Tenth",
      "ད": "Eleventieth",
      "ན": "Twelvetieth",
      "པ": "Thirtieth",
      "ཕ": "Fortieth",
      "བ": "Fiftieth",
      "མ": "Sixtieth",
      "ཙ": "Seventieth",
      "ཚ": "Eightieth",
      "ཛ": "Ninetieth",
      "ཝ": "Twenty",
      "ཞ": "Twenty one",
      "ཟ": "Twenty two",
      "འ": "Twenty three",
      "ཡ": "Twenty four",
      "ར": "Twenty five",
      "ལ": "Twenty six",
      "ཤ": "Twenty seven",
      "ས": "Twenty eight",
      "ཧ": "Twenty nine",
      "ཨ": "Thirty"
    };
    String? key = dict[name.trim()];
    message = "$key Letter of Dzongkha Character";
    accuracy = "$confidence%";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavBar(),
      appBar: AppBar(
        titleSpacing: 1,
        centerTitle: false,
        title: const Text(
          "Dzongkha Handwritten Character",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
            fontFamily: 'Roboto-Thin',
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 61, 85, 222),
        elevation: 0,
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: SizedBox(
                child: Text(
                  name,
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto-Medium',
                    fontSize: 60.0,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                message!,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontFamily: 'Roboto-Thin',
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                "Accuracy $accuracy",
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontFamily: 'Roboto-Thin',
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
